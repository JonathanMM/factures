const remote = require('electron').remote;
const main = remote.require('./main.js');
const fs = require('fs');

let bouton = document.createElement('button');
bouton.textContent = 'Configuration';
bouton.addEventListener('click', () => {
	main.ouvrirFenetre();
}, false);
document.body.appendChild(bouton);

function nettoyerInfo(info)
{
	return info.substring(1, info.length - 1);
}

fs.stat('../donnees/facture.csv', (err, stat) => {
	if(stat.isFile())
	{
		fs.readFile('../donnees/facture.csv', (err, fichier) => {
			let lignes = fichier.toString().split("\n");
			let i = 0;
			let informations = {};
			for(let ligne of lignes)
			{
				//Todo : Chercher la position des colonnes
				if(i != 0 && ligne != '')
				{
					let infos = ligne.split(';');
					let source = nettoyerInfo(infos[1]);
					let identifiant = nettoyerInfo(infos[2]);
					let montant = nettoyerInfo(infos[3]);
					let id = source+'-'+identifiant;
					if(!informations[id])
						informations[id] = parseFloat(montant);
					else
						informations[id] += parseFloat(montant);
				}
				i++;
			}
			let ul = document.createElement('ul');
			for(let id in informations)
			{
				var montant = informations[id];
				if(!isNaN(montant))
				{
					let li = document.createElement('li');
					li.innerHTML = id + ' : ' + montant.toLocaleString() + '€';
					ul.appendChild(li);
				}
			}
			document.body.appendChild(ul);
		});
	}
});
