const electron = require('electron');
const {app, BrowserWindow} = electron;

app.on('ready', () => {
	let fenetre = new BrowserWindow({width: 800, height: 600, autoHideMenuBar: true});
	fenetre.loadURL('file://'+__dirname+'/index.html');
	//fenetre.webContents.openDevTools();
});

exports.ouvrirFenetre = () => {
	let fenetre = new BrowserWindow({width: 400, height: 200});
	fenetre.loadURL('file://'+__dirname+'/config.html');
};
