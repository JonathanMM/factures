#!/usr/bin/python3
# -*- coding: utf-8 -*-
import re

def analyserMail(contenuMail, facInfos):
    if facInfos['expediteur'] == '"Free Mobile" <freemobile@free-mobile.fr>':
        #Free Mobile
        analyseFreeMobile(contenuMail, facInfos)
    elif facInfos['expediteur'] == 'Free Haut Debit <hautdebit@freetelecom.fr>':
        #Free Haut Débit
        analyseFreeHautDebit(contenuMail, facInfos)
    elif facInfos['expediteur'] == '"noco.tv" <abonnement@noco.tv>':
        #Noco
        analyseNoco(contenuMail, facInfos)
    else:
        facInfos['source'] = 'inconnu'

def analyseFreeMobile(contenuMail, facInfos):
    facInfos['source'] = 'Free Mobile'
    m = re.search(r"du (?P<date>\S+), d'un montant de (?P<montant>\S+)€ pour la ligne (?P<identifiant>[0-9 ]{14}).", contenuMail)
    if m is not None:
        infos = m.groupdict()
        cleInfos = infos.keys()
        if 'date' in cleInfos:
            facInfos['date'] = infos['date']
        if 'montant' in cleInfos:
            facInfos['montant'] = infos['montant']
        if 'identifiant' in cleInfos:
            facInfos['identifiant'] = infos['identifiant']

def analyseFreeHautDebit(contenuMail, facInfos):
    facInfos['source'] = 'Free'
    m = re.search(r"Vous trouverez en pièce jointe votre facture Free Haut Débit pour la ligne (?P<identifiant>[0-9]{10}).", contenuMail)
    if m is not None:
        infos = m.groupdict()
        cleInfos = infos.keys()
        if 'identifiant' in cleInfos:
            facInfos['identifiant'] = infos['identifiant']
    m = re.search(r"Le total de votre facture est de (?P<montant>\S+) Euros.", contenuMail)
    if m is not None:
        infos = m.groupdict()
        cleInfos = infos.keys()
        if 'montant' in cleInfos:
            facInfos['montant'] = infos['montant']

def analyseNoco(contenuMail, facInfos):
    facInfos['source'] = 'noco'
    m = re.search(r"(\t*)<h1 style=\"font-size:1.4em; color:#11AAEA\">Bonjour (?P<identifiant>\S+) !</h1>", contenuMail)
    if m is not None:
        infos = m.groupdict()
        cleInfos = infos.keys()
        if 'identifiant' in cleInfos:
            facInfos['identifiant'] = infos['identifiant']
