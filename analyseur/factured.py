#!/usr/bin/python3
# -*- coding: utf-8 -*-

import base64, os
from analyseurs import analyserMail

def analyserEntete(cle, valeur, facInfos, partie):
    if cle == None and valeur == None:
        return None
    #print(cle + ': ' + valeur)
    if cle == 'Date':
        facInfos['date'] = valeur
    elif cle == 'From':
        facInfos['expediteur'] = valeur
    elif cle == 'To':
        facInfos['destinataire'] = valeur
    elif cle == 'Message-Id':
        facInfos['id'] = valeur
    elif cle == 'Subject':
        facInfos['sujet'] = valeur
    elif cle == 'Content-Disposition':
        coupeValeur = valeur.split(';')
        prop = {}
        i = 0
        for bout in coupeValeur:
            if i != 0:
                coupeBout = bout.strip().split('=', 1)
                if coupeBout[0].strip() == 'filename':
                    facInfos['tmpFileName'] = coupeBout[1].strip('"')
            i += 1
    elif cle == 'Content-Type':
        coupeValeur = valeur.split(';')
        prop = {}
        i = 0
        for bout in coupeValeur:
            if i == 0:
                prop['type'] = bout.strip()
            else:
                coupeBout = bout.strip().split('=', 1)
                prop[coupeBout[0].strip()] = coupeBout[1].strip('"')
            i += 1

        if prop['type'] == 'multipart/mixed': #Mail en plusieurs bouts
            if partie == 0 and 'boundary' in prop.keys():
                facInfos['frontiere'] = prop['boundary']
                facInfos['multipart'] = True

        if not 'name' in prop.keys() and 'tmpFileName' in facInfos.keys():
            prop['name'] = facInfos['tmpFileName']
            facInfos['tmpFileName'] = None
        return prop

def enregistrerPieceJointe(pieceJointe, infosContentType, facInfos):
    pjfichier = open('../donnees/fichiers/' + infosContentType['name'], 'wb')
    pjfichier.write(base64.b64decode(pieceJointe))
    pjfichier.close()
    facInfos['pieceJointe'] = infosContentType['name']

def faireLigneCSV(facInfos):
    cles = facInfos.keys()
    l = ''
    if 'date' in cles:
        l += '"'+facInfos['date']+'"'
    l += ';'
    if 'source' in cles:
        l += '"'+facInfos['source']+'"'
    l += ';'
    if 'identifiant' in cles:
        l += '"'+facInfos['identifiant']+'"'
    l += ';'
    if 'montant' in cles:
        l += '"'+facInfos['montant']+'"'
    l += ';'
    if 'pieceJointe' in cles:
        l += '"'+facInfos['pieceJointe']+'"'
    return l

#MAIN
cheminDossier = '../donnees/email/atraiter/' #Doit finir par /
cheminTraite = '../donnees/email/traite/' #Doit finir par /

cheminCSV = '../donnees/facture.csv'
if os.path.isfile(cheminCSV): #Il existe, on écrit à la fin
    fichierCSV = open(cheminCSV, 'a')
else: #Nouveau fichier
    fichierCSV = open(cheminCSV, 'w')
    fichierCSV.write("Date;Source;Identifiant;Montant;\"Pièce Jointe\"\n")

for nomFichier in os.listdir(cheminDossier):
    if nomFichier != '.gitkeep':
        cheminFichier = cheminDossier + nomFichier
        facfichier = open(cheminFichier)
        print('Analyse de '+nomFichier)

        fichierLisible = False
        encodagePossible = ['iso-8859-15']
        iEncodagePossible = -1
        while not fichierLisible:
            try:
                facfichier.readline()
                facfichier.seek(0)
                fichierLisible = True
            except UnicodeDecodeError:
                iEncodagePossible += 1
                if iEncodagePossible >= len(encodagePossible):
                    print('Fichier non lisible')
                    quit()
                else:
                    facfichier = open(cheminFichier, 'r', -1, encodagePossible[iEncodagePossible])

        #On regarde où on en est
        entete = True #Ça commence toujours par des entêtes
        cle = None
        valeur = None
        contentType = None
        infosContentType = None
        partie = 0
        contenuMail = ''
        pieceJointe = ''

        #Extraits
        facInfos = {'multipart': False}

        #On lit le fichier ligne par ligne
        for facligne in facfichier.readlines():
            if entete:
                coupeLigne = facligne.split(': ', 1)
                if len(coupeLigne) == 1:
                    if facligne[0] == ' ' or facligne[0] == "\t":
                        valeur += "\n" + facligne.strip()
                    else:
                        infosLocales = analyserEntete(cle, valeur, facInfos, partie)
                        if infosLocales != None:
                            contentType = infosLocales['type']
                            infosContentType = infosLocales
                        entete = False #C'était pas une entête…
                else:
                    infosLocales = analyserEntete(cle, valeur, facInfos, partie)
                    if infosLocales != None:
                        contentType = infosLocales['type']
                        infosContentType = infosLocales
                    cle = coupeLigne[0].strip()
                    valeur = coupeLigne[1].strip()
            else:
                if facInfos['multipart'] and facInfos['frontiere'] in facligne:
                    entete = True #Nouvelle partie
                    cle = None
                    valeur = None
                    partie += 1
                    if contentType == 'text/plain' or contentType == 'text/html':
                        analyserMail(contenuMail, facInfos)
                        contenuMail = ''
                    if contentType == 'application/pdf': #on enregistre la piece jointe
                        enregistrerPieceJointe(pieceJointe, infosContentType, facInfos)
                        pieceJointe = ''
                elif contentType == 'text/plain' or contentType == 'text/html':
                    contenuMail += facligne
                elif contentType == 'application/pdf':
                    pieceJointe += facligne

        facfichier.close()
        if 'source' in facInfos.keys() and facInfos['source'] != 'inconnu':
            fichierCSV.write(faireLigneCSV(facInfos)+"\n")
            os.rename(cheminFichier, cheminTraite + nomFichier)

print('Fini')
