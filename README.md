Le projet facture est un petit projet qui permet de trier les mails contenant des factures

Fonctionnement
==============

* Mettre les emails au format .eml (Clic droit > Enregistrer sous… dans Thunderbird par exemple) dans le dossier donnees/email/atraiter
* Lancer analyseur/factured.py
* Le système va mettre les pièces jointes dans donnees/fichiers et la liste des factures dans facture.csv. Les mails traités seront déplacés dans donnees/email/traite
* Pour les mails non compris, ils ne seront pas déplacés, mais leur pièce jointe sera extraite

Type de mail pris en charge
===========================

* Free mobile
* Free
* noco.tv

Ajouter un type de mail
=======================

* Ouvrir un ticket dans le site du projet
* Coder soi-même :
 * Code à mettre dans analyseur/analyseurs.py
 * Valeurs à remplir dans facInfos : source (obligatoire), identifiant, montant, date (par défaut, est date du mail)

Autres infos
============
* 2016 JonathanMM
* https://framagit.org/JonathanMM/factures